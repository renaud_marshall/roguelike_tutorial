local globals = require "globals"
local board = require "board"

local player = {}

player.pos = board.get_player_pos()
player.pos.x = (player.pos.x * 64)
player.pos.y = (player.pos.y * 64)
player.image = love.graphics.newImage("person.png")

function player.move(direction)
	local pos_to_check = {}
    local board_pos = board.get_player_pos()
    pos_to_check.x = board_pos.x + direction.x
    pos_to_check.y = board_pos.y + direction.y
	local tile = board.get_entity_at(pos_to_check.x, pos_to_check.y)
	local is_tile_empty = tile == "empty"
	if is_tile_empty then
		board.set_player_pos(pos_to_check)
		player.pos = pos_to_check
		player.pos.x = (pos_to_check.x * 64)
		player.pos.y = (pos_to_check.y * 64)
	end
end

function player.draw()
    love.graphics.setColor(globals.colors(9))
    love.graphics.setLineWidth(4)
    love.graphics.setLineJoin("bevel")
    
    love.graphics.draw(player.image, player.pos.x, player.pos.y)
    
    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.setLineWidth(1)
    love.graphics.setLineJoin("miter")
end

return player
