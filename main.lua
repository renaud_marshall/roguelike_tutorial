function love.load()
    board = require "board"
    player = require "player"
    width, height = love.graphics.getDimensions()
    love.window.setTitle("RoguelikeTutorial")
end

function love.update(dt)
    
end

function love.keypressed(key, scancode, isrepeat)
    local direction = {}
    direction.x = 0
    direction.y = 0
    if key == "up" and isrepeat == false then
        direction.y = -1
        player.move(direction)
    elseif key == "down" and isrepeat == false then
        direction.y = 1
        player.move(direction)
    elseif key == "left" and isrepeat == false then
        direction.x = -1
        player.move(direction)
    elseif key == "right" and isrepeat == false then
        direction.x = 1
        player.move(direction)
    end
end

function love.draw()
    love.graphics.print("FPS: "..tostring(love.timer.getFPS()), width - 100, height - 50)
    board.draw()
    player.draw()
end
