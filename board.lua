local globals = require "globals"

local board = {}

board.width = 6
board.height = 6
board.board = {}
board.entities = {}
board.entity_image = love.graphics.newImage("skull-crossed-bones.png")

for i = 1, board.width do
    board.board[i] = {}

    for j = 1, board.height do
        board.board[i][j] = "empty"
    end
end

board.entities = {
    [1] = { 
        ["id"] = "player",
        ["x"] = 1,
        ["y"] = 1
    },
    [2] = { 
        ["id"] = "enemy",
        ["x"] = 5,
        ["y"] = 5
    },
    [3] = {
        ["id"] = "enemy",
        ["x"] = 6,
        ["y"] = 3
    },
}

function board.is_in_bounds(width, height)
    local is_within_width_bounds = (width >= 1) and (width <= board.width)
    local is_within_height_bounds = (height >= 1) and (height <= board.height)
    return is_within_width_bounds and is_within_height_bounds
end

function board.get_entity_at(width, height)
    if not board.is_in_bounds(width, height) then
        print("out of bounds")
        return nil
    end

    for i = 1, table.maxn(board.entities) do
        if board.entities[i]["x"] == width and board.entities[i]["y"] == height then
            print(board.entities[i]["id"])
            return board.entities[i]["id"]
        end
    end
    return board.board[width][height]
end

function board.get_entity_index_for(id)
    for i = 1, table.maxn(board.entities) do
        if board.entities[i]["id"] == id then
            return i
        end
    end

    print("not in the entities table")
    return nil
end

function board.get_entity_pos(entity_index)
    --TODO check if within bounds and return nil otherwise
    local pos = vmath.vector3(board.entities[entity_index]["x"], board.entities[entity_index]["y"], 0)
    return pos
end

function board.set_entity_pos(entity_index, pos)
    board.entities[entity_index]["x"] = pos.x
    board.entities[entity_index]["y"] = pos.y
end

function board.get_player_pos()
    return {["x"] = board.entities[1]["x"], ["y"] = board.entities[1]["y"]}
end

function board.set_player_pos(position)
    board.entities[1]["x"] = position.x
    board.entities[1]["y"] = position.y
end

function board.draw()
    -- layout empty tiles
    love.graphics.setColor(globals.colors(5))
    love.graphics.setLineWidth(4)
    love.graphics.setLineJoin("bevel")
    for i = 1, board.width do
		for j = 1, board.height do
			local pos = {
                ["x"] = 0,
                ["y"] = 0
            }
			pos.x = pos.x + (i * 64)
			pos.y = pos.y + (j * 64)
            love.graphics.rectangle("line", pos.x, pos.y, 64, 64)
		end
	end
    
    -- layout wall tiles
    love.graphics.setColor(globals.colors(7))
    for i = 0, board.width + 1 do
		for j = 0, board.height + 1 do
			if (i == 0) or (i == board.width + 1) or (j == 0) or (j == board.height + 1) then
                local pos = {
                ["x"] = 0,
                ["y"] = 0
                }
				pos.x = pos.x + (i * 64)
				pos.y = pos.y + (j * 64)
                love.graphics.rectangle("fill", pos.x, pos.y, 64, 64)
			end
		end
	end
    
    love.graphics.setColor(globals.colors(3))
    for i = 1, table.maxn(board.entities) do
		if board.entities[i]["id"] ~= "player" then
			local pos = {
                ["x"] = 0,
                ["y"] = 0
            }
			pos.x = (board.entities[i]["x"] * 64)
			pos.y = (board.entities[i]["y"] * 64)
            love.graphics.draw(board.entity_image, pos.x, pos.y)
		end
	end
    
    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.setLineWidth(1)
    love.graphics.setLineJoin("miter")
end

return board






